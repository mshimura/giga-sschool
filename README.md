# GIGAスクール構想のアンケート分析

## 目的
- 自然言語処理技術の分析方法を学ぶ
- 分析事業者による分析結果がついているのでそれらをお手本にできる．

## やったこと
- タブレット端末の使用に関すると思われるアンケート（"tablet_1of5_20210903.csv"）を適当にチョイス．
- "あなたがタブレットを学校などで使っているときに、困っていることはありますか"列を解析対象とした
- khcoderにより前処理→抽出後共起ネットワークの分析
- 「操作の困難さ、過度な使用規制、回線環境の悪さ、等を指摘するコメントが多い．」というボスコンの解析結果と同様の結果が得られた．

## 感想
- アンケートをまとめてcsvにした人が一番えらい．
- 分析事業者のやっていることはスライドがきれいで感動するが，手を動かしてみると中身はそんなに大したことない？
